package com.lillah.medjar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UserActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;

    //view objects
    EditText editTextNis,editTextNama, editTextKelas,editTextPassword ;
    Button buttonAddUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);


        mDatabase = FirebaseDatabase.getInstance().getReference("users");

        editTextNis = (EditText) findViewById(R.id.editTextNis);
        editTextNama = (EditText) findViewById(R.id.editTextNama);
        editTextKelas = (EditText) findViewById(R.id.editTextKelas);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonAddUser = (Button) findViewById(R.id.btnSimpan);

        buttonAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add();
            }
        });
    }


    private void add() {
        String nis = editTextNis.getText().toString();
        String nama = editTextNama.getText().toString().trim();
        String kelas = editTextKelas.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String nilai = "0";
        String lastLogin = "-";
        String role = "siswa";


        if (!TextUtils.isEmpty(nis)) {

            User user = new User(nis, nama, kelas, password,role, lastLogin, nilai);

            mDatabase.child(nis).setValue(user);

            editTextNis.setText("");
            editTextNama.setText("");
            editTextKelas.setText("");
            editTextPassword.setText("");

            Toast.makeText(this, "Data User Berhasil Disimpan", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Nis Harus Diisi", Toast.LENGTH_SHORT).show();
        }
    }
}

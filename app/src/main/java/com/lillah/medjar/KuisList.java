package com.lillah.medjar;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;


import android.widget.TextView;

import java.util.List;

public class KuisList extends ArrayAdapter<Kuis> {
    private Activity context;
    List<Kuis> questions;
    public static int[] skor;
    static int sum;

    public KuisList(Activity context, List<Kuis> questions) {
        super(context, R.layout.activity_list_kuis, questions);
        this.context = context;
        this.questions = questions;
    }

    public static int selesai(){
        sum = 0;
        for (int i = 1; i < skor.length ; i++){
            if (Integer.toString(skor[i]) != null) {
                sum = sum + skor[i];
            }else{
                sum = sum + 0;
            }
        }

        return sum;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.activity_list_kuis, null, true);

        TextView textViewSoal = (TextView) listViewItem.findViewById(R.id.textViewSoal);
        final Button optA = (Button) listViewItem.findViewById(R.id.optA);
        final Button optB = (Button) listViewItem.findViewById(R.id.optB);
        final Button optC = (Button) listViewItem.findViewById(R.id.optC);
        final Button optD = (Button) listViewItem.findViewById(R.id.optD);

        final Kuis kuis = questions.get(position);
        textViewSoal.setText(kuis.getQuestion());
        optA.setText(kuis.getOpta());
        optB.setText(kuis.getOptb());
        optC.setText(kuis.getOptc());
        optD.setText(kuis.getOptd());
        skor = new int[41];

        final KuisActivity kuisactivity = new KuisActivity();

        optA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optA.setBackgroundColor(Color.RED);
                optA.setTextColor(Color.parseColor("#ffffff"));
                optB.setTextColor(Color.DKGRAY);
                optC.setTextColor(Color.DKGRAY);
                optD.setTextColor(Color.DKGRAY);
                optB.setBackgroundResource(android.R.drawable.btn_default);
                optC.setBackgroundResource(android.R.drawable.btn_default);
                optD.setBackgroundResource(android.R.drawable.btn_default);

                optA.setEnabled(false);
                optB.setEnabled(true);
                optC.setEnabled(true);
                optD.setEnabled(true);

                String jwb = kuis.getAnswer();
                int nomor = Integer.parseInt(kuis.getQuestionId());

                if (jwb.equals("a")){
                    skor[nomor] = 1;
                }else{
                    skor[nomor] = 0;
                }

                kuisactivity.kuis_proses(nomor,skor[nomor]);

                Log.d(String.valueOf(skor[nomor]), "skor " + skor[nomor]+ " dari soal Nomor " + nomor);
            }
        });

        optB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optB.setBackgroundColor(Color.RED);
                optB.setTextColor(Color.parseColor("#ffffff"));
                optA.setTextColor(Color.DKGRAY);
                optC.setTextColor(Color.DKGRAY);
                optD.setTextColor(Color.DKGRAY);
                optA.setBackgroundResource(android.R.drawable.btn_default);
                optC.setBackgroundResource(android.R.drawable.btn_default);
                optD.setBackgroundResource(android.R.drawable.btn_default);

                optB.setEnabled(false);
                optA.setEnabled(true);
                optC.setEnabled(true);
                optD.setEnabled(true);

                String jwb = kuis.getAnswer();
                int nomor = Integer.parseInt(kuis.getQuestionId());

                if (jwb.equals("b")){
                    skor[nomor]=1;
                }else{
                    skor[nomor]=0;
                }

                kuisactivity.kuis_proses(nomor,skor[nomor]);

                Log.d(String.valueOf(skor[nomor]), "skor " + skor[nomor]+ " dari soal Nomor " + nomor);
            }
        });

        optC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optC.setBackgroundColor(Color.RED);
                optC.setTextColor(Color.parseColor("#ffffff"));
                optB.setTextColor(Color.DKGRAY);
                optA.setTextColor(Color.DKGRAY);
                optD.setTextColor(Color.DKGRAY);
                optB.setBackgroundResource(android.R.drawable.btn_default);
                optA.setBackgroundResource(android.R.drawable.btn_default);
                optD.setBackgroundResource(android.R.drawable.btn_default);

                optC.setEnabled(false);
                optB.setEnabled(true);
                optA.setEnabled(true);
                optD.setEnabled(true);

                String jwb = kuis.getAnswer();
                int nomor = Integer.parseInt(kuis.getQuestionId());

                if (jwb.equals("c")){
                    skor[nomor]=1;
                }else{
                    skor[nomor]=0;
                }

                kuisactivity.kuis_proses(nomor,skor[nomor]);

                Log.d(String.valueOf(skor[nomor]), "skor " + skor[nomor]+ " dari soal Nomor " + nomor);
            }
        });

        optD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optD.setBackgroundColor(Color.RED);
                optD.setTextColor(Color.parseColor("#ffffff"));
                optB.setTextColor(Color.DKGRAY);
                optC.setTextColor(Color.DKGRAY);
                optA.setTextColor(Color.DKGRAY);
                optB.setBackgroundResource(android.R.drawable.btn_default);
                optC.setBackgroundResource(android.R.drawable.btn_default);
                optA.setBackgroundResource(android.R.drawable.btn_default);

                optD.setEnabled(false);
                optB.setEnabled(true);
                optC.setEnabled(true);
                optA.setEnabled(true);

                String jwb = kuis.getAnswer();
                int nomor = Integer.parseInt(kuis.getQuestionId());

                Log.d(String.valueOf(nomor), "nomor soal " + nomor);

                if (jwb.equals("d")){
                    skor[nomor]=1;
                }else{
                    skor[nomor]=0;
                }

                kuisactivity.kuis_proses(nomor,skor[nomor]);
            }
        });

        return listViewItem;
    }

}


package com.lillah.medjar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LoginActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    EditText editTextUsername, editTextPass;
    public String userId;
    private boolean Registered;
    private  String Role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Registered = sharedPref.getBoolean("Registered", false);
        Role = sharedPref.getString("Role", "siswa");

        if (Registered)
        {
            if (Role.equals("siswa")) {
                startActivity(new Intent(this,SiswaActivity.class));
            }else{
                startActivity(new Intent(this,MainActivity.class));
            }
        }

        Button btnSignIn = (Button) findViewById(R.id.btnSignIn);
        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPass = (EditText) findViewById(R.id.editTextPass);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_user();
            }
        });
    }

    public void check_user() {
        final String username = editTextUsername.getText().toString();
        final String pswd = editTextPass.getText().toString();

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
        final String date = df.format(Calendar.getInstance().getTime());

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = sharedPref.edit();

        DatabaseReference firebaseRef = FirebaseDatabase.getInstance().getReference().child("users").child(username);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                User user = dataSnapshot.getValue(User.class);
                // [START_EXCLUDE]
                String password = user.getUserPassword();
                String role = user.getUserRole();
                // [END_EXCLUDE]

                if (password.equals(pswd)) {
                    if (role.equals("siswa")) {
                        mDatabase.child("users").child(username).child("userLastLogin").setValue(date);

                        editor.putBoolean("Registered", true);
                        editor.putString("username", username);
                        editor.putString("Role", role);
                        editor.apply();

                        startActivity(new Intent(LoginActivity.this, SiswaActivity.class));
                    }else {
                        mDatabase.child("users").child(username).child("userLastLogin").setValue(date);

                        editor.putBoolean("Registered", true);
                        editor.putString("Username", username);
                        editor.putString("Role", role);
                        editor.apply();

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }


                }else {
                    Toast.makeText(LoginActivity.this, "Username dan Password salah",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(null, "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]
                Toast.makeText(LoginActivity.this, "Failed to load post.",
                        Toast.LENGTH_SHORT).show();
                // [END_EXCLUDE]
            }
        };
        firebaseRef.addValueEventListener(postListener);
    }

    public void onBackPressed() {

    }

//    private final Runnable mRunnable = new Runnable() {
//        @Override
//        public void run() {
//            doubleBackToExitPressedOnce = false;
//        }
//    };
//
//    @Override
//    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            return;
//        }
//
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Klik tombol back 2x untuk keluar aplikasi", Toast.LENGTH_SHORT).show();
//
//        mHandler.postDelayed(mRunnable, 2000);
//    }
//
//    @Override
//    protected void onDestroy()
//    {
//        super.onDestroy();
//
//        if (mHandler != null) { mHandler.removeCallbacks(mRunnable); }
//    }
}

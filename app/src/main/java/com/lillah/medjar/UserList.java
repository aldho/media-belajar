package com.lillah.medjar;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class UserList extends ArrayAdapter<User> {
    private Activity context;
    List<User> users;

    public UserList(Activity context, List<User> users) {
        super(context, R.layout.activity_list_user, users);
        this.context = context;
        this.users = users;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.activity_list_user, null, true);

        TextView textViewNama = (TextView) listViewItem.findViewById(R.id.textViewNama);
        TextView textViewNis = (TextView) listViewItem.findViewById(R.id.textViewNis);
        TextView textViewKelas = (TextView) listViewItem.findViewById(R.id.textViewKelas);
        TextView textViewLastLogin = (TextView) listViewItem.findViewById(R.id.textViewLastLogin);
        TextView textViewNilai = (TextView) listViewItem.findViewById(R.id.textViewNilai);

        User user = users.get(position);
        textViewNama.setText(user.getUserNama());
        textViewNis.setText(user.getUserId());
        textViewKelas.setText(user.getUserKelas());

        if (user.getUserNilai() == null){
            textViewNilai.setText("Nilai : -");
        }else{
            textViewNilai.setText("Nilai : "+ user.getUserNilai());
        }

        textViewLastLogin.setText("Login terakhir : " + user.getUserLastLogin());

        return listViewItem;
    }



}

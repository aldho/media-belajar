package com.lillah.medjar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HasilActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    private  String username;

    TextView hasil;
    Button btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        hasil = (TextView) findViewById(R.id.textViewNilai);
        btnHome = (Button) findViewById(R.id.btnHome);

        Bundle b = new Bundle();
        b = getIntent().getExtras();
        String skor = b.getString("skor_akhir");

        hasil.setText(skor);


        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HasilActivity.this, SiswaActivity.class));
            }
        });

        simpanNilai(skor);


    }

    public void simpanNilai(String skor){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        username = sharedPref.getString("username",null);
        mDatabase.child("users").child(username).child("userNilai").setValue(skor);
    }

    public void onBackPressed() {
        startActivity(new Intent(HasilActivity.this, SiswaActivity.class));
    }
}

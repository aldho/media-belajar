package com.lillah.medjar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class SiswaActivity extends AppCompatActivity {
    private  String username;
    Button btnLogout;
    ImageView btnKd, btnModul, btnVideo, btnKuis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siswa);

        btnKd = (ImageView) findViewById(R.id.imageViewKd);
        btnModul = (ImageView) findViewById(R.id.imageViewModul);
        btnVideo = (ImageView) findViewById(R.id.imageViewVideo);
        btnKuis = (ImageView) findViewById(R.id.imageViewKuis);
        btnLogout = (Button) findViewById(R.id.btnLogout);

        btnKd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SiswaActivity.this, KdActivity.class));
            }
        });

        btnModul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SiswaActivity.this, ModulActivity.class));
            }
        });

        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SiswaActivity.this, VideoActivity.class));
            }
        });

        btnKuis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SiswaActivity.this, KuisActivity.class));
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
    }

    public void logout(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SiswaActivity.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        Intent i = new Intent(SiswaActivity.this,LoginActivity.class);
        startActivity(i);
    }
}

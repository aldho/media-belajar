package com.lillah.medjar;

public class Kuis {
    private String questionId, question, opta, optb, optc, optd, answer;

    public Kuis() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Kuis(String id, String question, String opta, String optb, String optc, String optd, String answer) {
        this.questionId = id;
        this.question = question;
        this.opta = opta;
        this.optb = optb;
        this.optc = optc;
        this.optd = optd;
        this.answer = answer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getQuestion() {
        return question;
    }

    public String getOpta() {
        return opta;
    }

    public String getOptb() {
        return optb;
    }

    public String getOptc() {
        return optc;
    }

    public String getOptd() {
        return optd;
    }

    public String getAnswer() {
        return answer;
    }

}

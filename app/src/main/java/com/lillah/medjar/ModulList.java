package com.lillah.medjar;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ModulList extends ArrayAdapter<Modul> {
    private Activity context;
    List<Modul> moduls;

    public ModulList(Activity context, List<Modul> moduls) {
        super(context, R.layout.activity_modul_list, moduls);
        this.context = context;
        this.moduls = moduls;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.activity_modul_list, null, true);

        TextView textViewJudul = (TextView) listViewItem.findViewById(R.id.textViewJudul);
        TextView textViewKonten = (TextView) listViewItem.findViewById(R.id.textViewKonten);
        ImageView imageView = (ImageView) listViewItem.findViewById(R.id.imageView);

        Modul modul = moduls.get(position);

        String url = modul.getModulGambar();

        Log.d("url", "getView: " + url);
        textViewJudul.setText(modul.getModulJudul());
        textViewKonten.setText(modul.getModulkonten());
        Picasso.with(context).load(url).into(imageView);

        return listViewItem;
    }



}

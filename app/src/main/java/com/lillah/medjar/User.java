package com.lillah.medjar;

public class User {
    private String userId,userNama,userKelas,userPassword,userRole,userLastLogin, userNilai;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String nis, String nama, String kelas, String password, String role, String LastLogin, String nilai) {
        this.userId = nis;
        this.userNama = nama;
        this.userKelas = kelas;
        this.userPassword = password;
        this.userRole = role;
        this.userLastLogin = LastLogin;
        this.userNilai = nilai;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserNama() {
        return userNama;
    }

    public String getUserKelas() {
        return userKelas;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public String getUserLastLogin() {
        return userLastLogin;
    }

    public String getUserNilai() {
        return userNilai;
    }

}

package com.lillah.medjar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class KuisActivity extends AppCompatActivity {
    public KuisList list;
    private DatabaseReference mDatabase;
    boolean doubleBackToExitPressedOnce = false;

    public int skors[] = new int[41];
    public int nilai = 0;

    int nmr_soal;
    ArrayList<Integer> soal_selesai;
    int skor=0;
    Random r;
    String jawaban;
    Button btnA,btnB,btnC,btnD,btnN;
    TextView soal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis);
        r = new Random();
          btnA = (Button) findViewById(R.id.btnA);
          btnB = (Button) findViewById(R.id.btnB);
          btnC = (Button) findViewById(R.id.btnC);
          btnD = (Button) findViewById(R.id.btnD);
          btnN = (Button) findViewById(R.id.btnNext);
         soal = (TextView) findViewById(R.id.txtSoal);
        final TextView hasil = (TextView) findViewById(R.id.txtHasil);

        soal_selesai=new ArrayList<>();
        
        btnA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jawaban.equals("a")){
                    skor+=1;
                    hasil.setText("Benar");
                }else{
                    hasil.setText("Salah");
                }
                btnA.setEnabled(false);
                btnB.setEnabled(false);
                btnC.setEnabled(false);
                btnD.setEnabled(false);
                btnN.setEnabled(true);
            }
        });

        btnB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jawaban.equals("b")){
                    skor+=1;
                    hasil.setText("Benar");
                }else{
                    hasil.setText("Salah");
                }

                btnA.setEnabled(false);
                btnB.setEnabled(false);
                btnC.setEnabled(false);
                btnD.setEnabled(false);
                btnN.setEnabled(true);
            }
        });

        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jawaban.equals("c")){
                    skor+=1;
                    hasil.setText("Benar");
                }else{
                    hasil.setText("Salah");
                }


                btnA.setEnabled(false);
                btnB.setEnabled(false);
                btnC.setEnabled(false);
                btnD.setEnabled(false);
                btnN.setEnabled(true);
            }
        });

        btnD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jawaban.equals("d")){
                    skor+=1;
                    hasil.setText("Benar");
                }else{
                    hasil.setText("Salah");
                }
                btnA.setEnabled(false);
                btnB.setEnabled(false);
                btnC.setEnabled(false);
                btnD.setEnabled(false);
                btnN.setEnabled(true);
            }
        });
        btnN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(intent);
                hasil.setText("");
                Log.e("A",nmr_soal+"|"+skor+"|"+soal_selesai.size());
                next_soal();

            }
        });
        mDatabase = FirebaseDatabase.getInstance().getReference();
        next_soal();

    }

    private void next_soal(){
        nmr_soal=r.nextInt(40)+1;
        if(soal_selesai.size()==40)
        {

            Intent intent2 = new Intent(KuisActivity.this, HasilActivity.class);
            intent2.putExtra("skor_akhir", skor + "");
            startActivity(intent2);
        }else {
            while (soal_selesai.contains(nmr_soal)) {
                nmr_soal = r.nextInt(40) + 1;
            }
            soal_selesai.add(nmr_soal);
            //intent.putExtra("question", nmr_soal);
            DatabaseReference ref = mDatabase.child("questions");

            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String nomor_soal = dataSnapshot.child("question" + nmr_soal).child("question").getValue(String.class);
                    String opta = dataSnapshot.child("question" + nmr_soal).child("opta").getValue(String.class);
                    String optb = dataSnapshot.child("question" + nmr_soal).child("optb").getValue(String.class);
                    String optc = dataSnapshot.child("question" + nmr_soal).child("optc").getValue(String.class);
                    String optd = dataSnapshot.child("question" + nmr_soal).child("optd").getValue(String.class);
                    jawaban = dataSnapshot.child("question" + nmr_soal).child("answer").getValue(String.class);

                    btnN.setEnabled(false);
                    btnA.setEnabled(true);
                    btnB.setEnabled(true);
                    btnC.setEnabled(true);
                    btnD.setEnabled(true);
                    btnN.setEnabled(false);

                    soal.setText(nomor_soal);
                    btnA.setText(opta);
                    btnB.setText(optb);
                    btnC.setText(optc);
                    btnD.setText(optd);
                    //final Intent intent = new Intent(KuisActivity.this, KuisActivity.class);


//
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            ref.addListenerForSingleValueEvent(eventListener);
        }

    }

    public int hitung_skor(){
        nilai = list.selesai();
        return nilai;
    }

    public void kuis_proses(int nomor, int jawaban){
        if (jawaban == 1){
            skors[nomor] = 1;
        }else{
            skors[nomor] = 0;
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Klik 2x untuk keluar", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}

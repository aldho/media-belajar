
package com.lillah.medjar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ModulActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;

    ListView listViewModuls;

    //a list to store all the artist from firebase database
    List<Modul> moduls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modul);

        mDatabase = FirebaseDatabase.getInstance().getReference("moduls");
        listViewModuls = (ListView) findViewById(R.id.listViewModuls);

        moduls = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //clearing the previous artist list
                moduls.clear();

                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting artist
                    Modul modul = postSnapshot.getValue(Modul.class);
                    //adding artist to the list
                    moduls.add(modul);
                }

                //creating adapter
                ModulList modulAdapter = new ModulList(ModulActivity.this, moduls);
                //attaching adapter to the listview
                listViewModuls.setAdapter(modulAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

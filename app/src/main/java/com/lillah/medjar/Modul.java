package com.lillah.medjar;

public class Modul {
    private String modulId,modulJudul,modulKonten, modulGambar;



    public Modul() {
        // Default constructor required for calls to DataSnapshot.getValue(Modul.class)
    }

    public Modul(String id, String judul, String konten, String gambar) {
        this.modulId = id;
        this.modulJudul = judul;
        this.modulKonten = konten;
        this.modulGambar = gambar;
    }

    public String getModulId() { return modulId; }

    public String getModulJudul() {
        return modulJudul;
    }

    public String getModulkonten() {
        return modulKonten;
    }

    public String getModulGambar() {
        return modulGambar;
    }

}
